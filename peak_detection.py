#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import math

pi = math.pi

fs = 10e3

def sinc(t,f):
    if t == 0:
        return 1
    
    return math.sin(f*t * pi)/(f*t* pi)

sfunc = []
f = fs/4

for t in range(-int(fs/f),int(fs/f)):
    sfunc += [sinc(t*1/fs, f)]

vals = [0]*200 + [1]*120
vals = np.array(vals)
np.random.shuffle(vals)
sfunc = np.array(sfunc)

data = np.convolve(vals, sfunc)

plt.plot(data)

windowsize = 20
currval = 0
prev = 0
window = []
peaks = []
avg = []
for t in data:
    window += [t.tolist()-prev]
    currval += 1
    if currval > windowsize:
        window.pop(0)
        currval -= 1
    print(window)
    dev2 = np.std(window)*3
    mean = np.mean(window)
    if t > dev2:
        peaks += [1]
    else:
        peaks += [0]
    avg += [mean]
    prev=t

plt.plot(peaks)
plt.plot(avg)
plt.show()




print(vals)